### Set-up https keys

1) Create `sslcert` folder inside `config` folder
2) Run next command:
`openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem`
3) Open `https://localhost:8443/` for test