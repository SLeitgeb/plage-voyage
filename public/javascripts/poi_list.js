Vue.component('PoiList', {
  template: '#poi_list',
  data: function () {
    return {
      pois: [
        {
          title: "Wake Up Academy",
          description: "Awesome place to relax and enjoy the amazing water sports.",
          position: [56.921493, 24.111233]
        }
      ],
      expanded_poi: null
    };
  },
  methods: {
    filter() {
      this.$emit('filter', this.filters);
    },
    expandPoi(poi){
      this.expanded_poi = poi;
    }
  },
  created() {
    this.$eventHub.$on('amenities_received', (list) => {
        this.pois = list;
    });
  }
});
