const MapView = {
    template: '#map',
    data: () => {
        return {
            click_pos: null,
            layers: {},
            sources: {},
            filters: null,
            lon: null,
            lat: null,
            pin_putting: false,
            coords: [0, 0]
        }
    },
    computed: {
        coordinates() {
            if (this.click_pos == null) return '';
            return `${this.click_pos.longitude}, ${this.click_pos.latitude}`
        }
    },
    methods: {
        createMap() {
            this.searchFeature = new ol.Feature({
                geometry: new ol.geom.Point([0, 0])
            });

            this.searchFeature.setStyle(new ol.style.Style({
                image: new ol.style.Icon(({
                    anchor: [0.5, 64],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    src: 'images/pin_white_blue64_2.png'
                })),
                text: new ol.style.Text({
                    text: ' Your location ',
                    font: 'bold 1.5em Roboto, sans-serif',
                    offsetY: -77,
                    backgroundFill: new ol.style.Fill({ color: 'rgb(230, 230, 230)' }),
                    fill: new ol.style.Fill({
                        color: 'rgb(31, 93, 155)'
                    })
                })
            }));

            var searchSource = new ol.source.Vector({
                features: [this.searchFeature]
            });

            var searchLayer = new ol.layer.Vector({
                source: searchSource
            });

            this.meteostationSource = new ol.source.Vector({
                features: []
            });

            var meteostationLayer = new ol.layer.Vector({
                source: this.meteostationSource
            });

            this.map = new ol.Map({
                target: 'olmap',
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    }),
                    //vectorLayer,
                    searchLayer,
                    meteostationLayer
                ],
                view: new ol.View({
                    center: ol.proj.fromLonLat([24.806933516196853, 56.7315480551824]),
                    zoom: 8
                })
            });
            setTimeout(() => {
                this.map.updateSize();
            }, 500);


            var selectClick = new ol.interaction.Select({
                condition: ol.events.condition.click
            });
            this.map.addInteraction(selectClick);

            selectClick.on('select', (e) => {
                if(e.selected.length == 0) return;
                var ext = e.selected[0].getGeometry().getCoordinates();
                axios.get(`http://195.244.151.5:5000/get_amenities?longitude=${ext[0]}&latitude=${ext[1]}&max_distance=2`)
                    .then((response) => {
                        console.log('Comparison generated for', ext);
                        console.log(response.data);
                        if(response.data.features == null)
                            this.$eventHub.$emit('amenities_received', []);
                        else
                            this.$eventHub.$emit('amenities_received', response.data.features.map(d => {
                                return {
                                    type: d.properties.amenity,
                                    title: d.properties.name, 
                                    position: d.geometry.coordinates
                                }
                            }));
                    })
            });

            this.map.on("click", (evt) => {
                if (this.pin_putting) {
                    var coords = evt.coordinate;
                    this.coords = coords;
                    this.setCurrentPosition(this.coords[0], this.coords[1], false);
                    this.$eventHub.$emit('main_map_clicked', { coords: this.coords });
                    this.pin_putting = false;
                }
            });
        },

        getData() {
            for (key in this.sources) {
                this.sources[key].clear();
                this.sources[key].refresh();
            }
        },

        hideOverlay() {
            this.overlayVisible = false;
            localStorage.overlayHidden = true;
            document.documentElement.style['overflow-y'] = 'auto';
        },

        putPin() {
            this.pin_putting = true;
        },

        setCurrentPosition(lon, lat, centerTo) {
            var cords = [lon, lat];
            this.searchFeature.getGeometry().setCoordinates(cords);
            if (typeof centerTo == 'undefined' || centerTo == true) {
                this.map.getView().setCenter(cords);
                var zoomLevel = 10;
                this.map.getView().setZoom(zoomLevel);
            }
            this.getData();
        },

        findMe() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    this.coords = ol.proj.transform([position.coords.longitude, position.coords.latitude], 'EPSG:4326', this.map.getView().getProjection());
                    this.setCurrentPosition(this.coords[0], this.coords[1]);
                });
            } else {
                alert("Geolocation is not supported by this browser.");
            }
        },

        createGeojsonLayer(layer_name, url, style) {
            var vm = this;
            var src = new ol.source.Vector({
                url: url,
                format: new ol.format.GeoJSON({}),
                loader: function (extent, resolution, projection) {
                    axios.get(url, {
                        params: Object.assign(vm.filters, { longitude: vm.coords[0], latitude: vm.coords[1] })
                    })
                        .then(response => {
                            var features = src.getFormat().readFeatures(response.data);
                            src.addFeatures(features);
                        })
                },
                strategy: ol.loadingstrategy.all
            });

            this.sources[layer_name] = src;

            this.layers[layer_name] = new ol.layer.Vector({
                title: 'Plaze',
                source: src,
                style: style
            });

            this.map.addLayer(this.layers[layer_name]);
        }
    },
    created() {
        this.$eventHub.$on('goto_location', (data) => {
            this.setCurrentPosition(parseFloat(data.lon), parseFloat(data.lat));
        });
        this.$eventHub.$on('do_hide_overlay', (t) => {
            this.hideOverlay();
        });
    },
    mounted() {
        this.createMap();
        if (localStorage.overlayHidden)
            this.overlayVisible = false;
        else
            document.documentElement.style['overflow-y'] = 'hidden';
        this.map.render();
        this.createGeojsonLayer('amenities', 'data/plaze.geojson', [new ol.style.Style({
            image: new ol.style.Circle({
                radius: 6,
                stroke: new ol.style.Stroke({
                    color: 'white',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'green'
                })
            })
        })]);
        this.createGeojsonLayer('coastline', 'http://195.244.151.5:5000/get_coastline', [new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'yellow',
                width: 6
            })
        })]);
        this.createGeojsonLayer('beaches', 'http://195.244.151.5:5000/get', [new ol.style.Style({
            image: new ol.style.Circle({
                radius: 10,
                stroke: new ol.style.Stroke({
                    color: 'white',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'orange'
                })
            })
        })]);

        this.$eventHub.$on('filters_changed', (t) => {
            this.filters = t;
            this.getData();
        });
    },
    beforeDestroy() {
        this.$eventHub.$off('filters_changed');
    }
}
