Vue.prototype.$eventHub = new Vue();
new Vue({
  router: router,
  el: '#app',
  data: {
    mini: true,
    searchText: null,
    searchResults: [],
    searchResultsVisible: false,
    error: null,
    listDialog: false,
    leftDrawer: false,
    rightDrawer: true,
    items: [
      { icon: 'home', title: 'Home', to: '/' },
    ],
    filters: {},
    title: 'Plaž Vojaž',
    render: h => h(App)
  },
  watch: {

  },
  methods: {
    search: _.debounce(function () {
      axios.get(`https://nominatim.openstreetmap.org/search`, {
        params: { q: this.searchText, format: 'json' }
      })
        .then(response => {
          this.searchResults = response.data;
          this.searchResultsVisible = true;
        }).catch((err) => this.error = err)
    }, 500),
    gotoLocation(lat, lon) {
      this.rightDrawer = true;
      this.$eventHub.$emit('goto_location', { lat: this.lat, lon: this.lon });
    },
    loaded() {
      
    },
    filter(filters, dd){
      this.filters = filters;
      this.$eventHub.$emit('filters_changed', filters);
    }

  },
  mounted() {
    this.loaded();
    document.body.style = "display: auto";
    router.beforeEach((to, from, next) => {
      next();
    })
  },
  created() {
  },
  beforeDestroy() {
    
  },
});

var utils = {
  rainbow(numOfSteps, step, opacity) {
      // based on http://stackoverflow.com/a/7419630
      // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distiguishable vibrant markers in Google Maps and other apps.
      // Adam Cole, 2011-Sept-14
      // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
      var r, g, b;
      var h = step / (numOfSteps * 1.00000001);
      var i = ~~(h * 4);
      var f = h * 4 - i;
      var q = 1 - f;
      switch (i % 4) {
          case 2:
              r = f, g = 1, b = 0;
              break;
          case 0:
              r = 0, g = f, b = 1;
              break;
          case 3:
              r = 1, g = q, b = 0;
              break;
          case 1:
              r = 0, g = 1, b = q;
              break;
      }
      var c = "rgba(" + ~~(r * 235) + "," + ~~(g * 235) + "," + ~~(b * 235) + ", " + opacity + ")";
      return (c);
  }
}