Vue.component('FilterPanel', {
    template: '#right_drawer',
    data: function () {
        return {
            activities: ['Swimming', 'Fitness', 'Water vehicles', 'Fishing', 'Sightseeing'],
            location_types: ['Lake', 'Sea', 'River'],
            charge_free: ['Free', 'Paid'],
            amenities: ['Wc', 'Shower', 'Changing room'],
            food: ['Bistro', 'Snacks', 'Restaurant'],
            filters: {
                activity: 'Swimming',
                food: [],
                charge: ['Free', 'Paid'],
                amenities: null,
                location_type: 'Sea',
                disability_friendly: false,
                parking_type: [],
                polluted: false,
                max_distance: 20,
                campsite: false
            }
        };
    },
    watch: {
        filters: {
            handler: function (newQuestion, oldQuestion) {
                this.$emit('filter', newQuestion);
            }, deep: true
        }
    },
    methods: {
        filter() {
            this.$emit('filter', this.filters);
        }
    },
    created() {

    },
    mounted() {
        Vue.nextTick()
            .then(() => {
                this.filter();
            })
    }
});
